# [league](https://phppackages.org/s/league)/[oauth2-server](https://phppackages.org/p/league/oauth2-server)

OAuth 2.0 Server https://oauth2.thephpleague.com

Unofficial howto and demo

## Unofficial documentation
* [*League OAuth 2.0 server with Symfony – Introduction*](https://www.thinktocode.com/2018/10/18/league-oauth-2-0-server-with-symfony-introduction/)
* 2018 Jeffrey Verreckt